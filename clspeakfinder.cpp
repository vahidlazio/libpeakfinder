/*
 * clsPeakFinder.cpp
 *
 *  Created on: 19 Dec 2010
 *      Author: vahid
 */
#include "clspeakfinder.h"
namespace Finder
{
clsPeakFinder::clsPeakFinder(QObject* _Parent) :
    QObject(_Parent)
{
    this->MaxAndMinDiff = 4;
}
void clsPeakFinder::setMaxAndMinDiff(double _Diff)
{
    this->MaxAndMinDiff = _Diff;
}
double clsPeakFinder::maxAndMinDiff()
{
}
QList<QPointF> clsPeakFinder::startPeakFinder(QList<QPointF> _Points)
{
    this->LastMinPeak = _Points.at(0).y();
    return this->parseSignalAndFindPositivePeaks(_Points);
}
QList<QPointF> clsPeakFinder::parseSignalAndFindPositivePeaks(QList<QPointF> _Points)
{
    QList<QPointF> PeakPoints;
    int CurrentIndex = 1;
    int EndWhile = _Points.size();
    QPointF SubmitedNeg;
    SubmitedNeg.setX(_Points.at(0).x());
    SubmitedNeg.setY(-250);
    QPointF CurrentNeg;
    CurrentNeg.setX(_Points.at(0).x());

    CurrentNeg.setY(_Points.at(0).y());
    QList<QPointF> Positives;
    while (CurrentIndex < EndWhile)
    {
        if (CurrentIndex + 1 < EndWhile)
        {
            //qDebug() << "hi while parse peak" << _Points.at(CurrentIndex + 1).y() << _Points.at(CurrentIndex).y() << _Points.at(CurrentIndex - 1).y();
            if ((_Points.at(CurrentIndex + 1).y() - _Points.at(CurrentIndex).y()) * (_Points.at(CurrentIndex).y() - _Points.at(CurrentIndex - 1).y()) < 0)
            {
                if ((_Points.at(CurrentIndex + 1).y() - _Points.at(CurrentIndex).y()) > 0)
                {
                    //Min Peak
                    if ((_Points.at(CurrentIndex).y() - SubmitedNeg.y() < this->MaxAndMinDiff / 4) || (SubmitedNeg.y() - _Points.at(CurrentIndex).y() > this->MaxAndMinDiff / 4))
                    {
                        //SUMBIT LAST POS AND UPDATE NEG
                        //qDebug() << "MoreNeg" << "CURRENTPOINT:" << _Points.at(CurrentIndex).y() << "NEG:" << CurrentNeg.y() << "SUBMITEDNEG:" << SubmitedNeg.y() << "POINT:" << _Points.at(CurrentIndex).y()
                        //								<< _Points.at(CurrentIndex).x();
                        QPointF Temp = this->retMaxBetween(Positives, SubmitedNeg, _Points.at(CurrentIndex));
                        if (Temp.x() != 0 && Temp.y() != 0)
                        {
                            PeakPoints.push_back(Temp);
                            SubmitedNeg.setX(_Points.at(CurrentIndex).x());
                            SubmitedNeg.setY(_Points.at(CurrentIndex).y());
                            Positives.clear();
                        }
                        CurrentNeg.setX(_Points.at(CurrentIndex).x());
                        CurrentNeg.setY(_Points.at(CurrentIndex).y());

                    }
                    if (_Points.at(CurrentIndex).y() <= CurrentNeg.y())
                    {
                        //UPDATE CURRENT NEG.
                        CurrentNeg.setX(_Points.at(CurrentIndex).x());
                        CurrentNeg.setY(_Points.at(CurrentIndex).y());
                    }
                }
                else
                {
                    //MaxPeak
                    if (_Points.at(CurrentIndex).y() - this->MaxAndMinDiff > CurrentNeg.y())
                    {
                        //UPDATE NEG SUBMIT LAST POS.MARK this point that could be a TRUE PEAK!!!!!!!!
                        Positives.push_back(_Points.at(CurrentIndex));
                    //	qDebug() << "POSPEAKCROSSED" << "NEG:" << CurrentNeg.y() << "SUBMITEDNEG:" << SubmitedNeg.y() << "POINT:" << _Points.at(CurrentIndex).y() << _Points.at(CurrentIndex).x();
                        //SUBMIT LAST POS
                        QPointF Temp2 = this->retMaxBetween(Positives, SubmitedNeg, CurrentNeg);
                        if (Temp2.x() != 0 && Temp2.y() != 0)
                        {
                            PeakPoints.push_back(Temp2);
                            Positives.clear();
                        }
                        SubmitedNeg.setX(CurrentNeg.x());
                        SubmitedNeg.setY(CurrentNeg.y());
                    }
                }
            }
            CurrentIndex++;
        }
        else
            return PeakPoints;
    }
    return PeakPoints;
}
QPointF clsPeakFinder::retMaxBetween(QList<QPointF> _Positives, QPointF _Start, QPointF _Stop)
{
    QPointF Max;
    Max.setX(0);
    Max.setY(-250);
    //	qDebug() << _Positives.size() << "XXXXXXXXXXXXX";
    for (int i = 0; i < _Positives.size(); i++)
    {
        //	qDebug() << _Positives.at(i).x() << _Start.x() << _Stop.x();
        if (_Positives.at(i).x() < _Stop.x() && _Positives.at(i).x() > _Start.x())
        {
            //	qDebug() << "helo";
            if (_Positives.at(i).y() > Max.y())
            {
                Max = _Positives.at(i);
            }
        }
    }
    //qDebug() << Max.x() << Max.y() << "YYYYYYYYYYYY";
    return Max;
}
}

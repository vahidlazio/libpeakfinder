#-------------------------------------------------
#
# Project created by QtCreator 2014-08-13T07:56:43
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = LibPeakFinder
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = lib


SOURCES += main.cpp \
    clspeakfinder.cpp

HEADERS += \
    clspeakfinder.h

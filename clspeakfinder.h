/*
 * clsPeakFinder.h
 *
 *  Created on: 19 Dec 2010
 *      Author: vahid
 */

#ifndef FINDER_CLSPEAKFINDER_H_
#define FINDER_CLSPEAKFINDER_H_
#include <QtCore>
#include <QPointF>
namespace Finder
{
class clsPeakFinder: public QObject
{
    Q_OBJECT
    private:
        double LastMinPeak;
        double MaxAndMinDiff;
    public:
        clsPeakFinder(QObject* _Parent = 0);
        void setMaxAndMinDiff(double _Diff);
        QPointF retMaxBetween(QList<QPointF> _Positives, QPointF _Start, QPointF _Stop);
        double maxAndMinDiff();
        QList<QPointF> startPeakFinder(QList<QPointF> _Points);
        QList<QPointF> parseSignalAndFindPositivePeaks(QList<QPointF> _Points);
};
}
#endif /* FINDER_CLSPEAKFINDER_H_ */

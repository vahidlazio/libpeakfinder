# README #

Peak detection is a pivotal first step in bio marker discovery from mass spectrometry (MS) data and can significantly influence the results of downstream data analysis steps. 